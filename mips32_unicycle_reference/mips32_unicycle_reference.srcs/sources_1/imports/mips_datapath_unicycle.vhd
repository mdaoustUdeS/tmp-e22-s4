---------------------------------------------------------------------------------------------
--
--	Universit� de Sherbrooke 
--  D�partement de g�nie �lectrique et g�nie informatique
--
--	S4i - APP4 
--	
--
--	Auteurs: 		Marc-Andr� T�trault
--					Daniel Dalle
--					S�bastien Roy
-- 
---------------------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MIPS32_package.all;


entity mips_datapath_unicycle is
Port ( 
	clk 			: in std_ulogic;
	reset 			: in std_ulogic;

	i_alu_funct   	: in std_ulogic_vector(4 downto 0);
	i_RegWrite    	: in std_ulogic;
	i_RegDst      	: in std_ulogic;
	i_MemtoReg    	: in std_ulogic;
	i_branch      	: in std_ulogic;
	i_ALUSrc      	: in std_ulogic;
	i_MemRead 		: in std_ulogic;
	i_MemWrite	  	: in std_ulogic;
	i_MemReadWide   : in std_ulogic;
	i_MemWriteWide  : in std_ulogic;
	i_vecteur       : in std_ulogic;
	i_rotation      : in std_ulogic;

	i_jump   	  	: in std_ulogic;
	i_jump_register : in std_ulogic;
	i_jump_link   	: in std_ulogic;
	i_alu_mult      : in std_ulogic;
	i_mflo          : in std_ulogic;
	i_mfhi          : in std_ulogic;
	i_SignExtend 	: in std_ulogic;
	
	i_Movnv         : in std_ulogic;
	i_Movzv         : in std_ulogic;

	o_Instruction 	: out std_ulogic_vector (31 downto 0);
	o_PC		 	: out std_ulogic_vector (31 downto 0)
);
end mips_datapath_unicycle;

architecture Behavioral of mips_datapath_unicycle is


component MemInstructions is
    Port ( i_addresse : in std_ulogic_vector (31 downto 0);
           o_instruction : out std_ulogic_vector (31 downto 0));
end component;

component MemDonneesWideDual is
Port ( 
	clk 		: in std_ulogic;
	reset 		: in std_ulogic;
	i_MemRead	: in std_ulogic;
	i_MemWrite 	: in std_ulogic;
    i_Addresse 	: in std_ulogic_vector (31 downto 0);
	i_WriteData : in std_ulogic_vector (31 downto 0);
    o_ReadData 	: out std_ulogic_vector (31 downto 0);
	i_MemReadWide       : in std_ulogic;
	i_MemWriteWide 		: in std_ulogic;
	i_WriteDataWide 	: in std_ulogic_vector (127 downto 0);
    o_ReadDataWide 		: out std_ulogic_vector (127 downto 0)
);
end component;

component BancRegistres is
Port ( 
    clk : in std_ulogic;
    reset : in std_ulogic;
    i_RS1 : in std_ulogic_vector (4 downto 0);
    i_RS2 : in std_ulogic_vector (4 downto 0);
    i_Wr_DAT : in std_ulogic_vector (31 downto 0);
    i_Wr_DAT_vecteur : in  std_ulogic_vector (127 downto 0);
    i_WDest   : in  std_ulogic_vector (4 downto 0);
    i_WE 	 : in  std_ulogic;
    i_Move 	 : in  std_ulogic_vector (3 downto 0);
    i_vecteur : in std_ulogic;
    o_RS1_DAT : out std_ulogic_vector (31 downto 0);
    o_RS2_DAT : out std_ulogic_vector (31 downto 0);
    o_RS3_DAT : out std_ulogic_vector (31 downto 0);
    o_RS4_DAT : out std_ulogic_vector (31 downto 0);
    o_RS5_DAT : out std_ulogic_vector (31 downto 0);
    o_RS6_DAT : out std_ulogic_vector (31 downto 0);
    o_RS7_DAT : out std_ulogic_vector (31 downto 0);
    o_RS8_DAT : out std_ulogic_vector (31 downto 0)
    );
end component;

component alu is
Port ( 
    i_a			: in std_ulogic_vector (31 downto 0);
    i_b			: in std_ulogic_vector (31 downto 0);
    i_alu_funct	: in std_ulogic_vector (4 downto 0);
    i_shamt		: in std_ulogic_vector (4 downto 0);
    o_result	: out std_ulogic_vector (31 downto 0);
    o_multRes    : out std_ulogic_vector (63 downto 0);
    o_zero		: out std_ulogic
    );
end component;
component ResultVecteur is
 Port (  i_MIPS_funct  : in std_ulogic_vector (5 downto 0);
       rotv : in std_ulogic := '0';
       resALU1 : in std_ulogic_vector (31 downto 0) := (others => '0');
       resALU2 : in std_ulogic_vector (31 downto 0) := (others => '0');
       resALU3 : in std_ulogic_vector (31 downto 0) := (others => '0');
       resALU4 : in std_ulogic_vector (31 downto 0) := (others => '0');
       resVecteur : out std_ulogic_vector (127 downto 0);-- := (others => '0');
       o_Move   : out std_ulogic_vector (3 downto 0) := "0000");
end component;

	constant c_Registre31		 : std_ulogic_vector(4 downto 0) := "11111";
	signal s_zero        : std_ulogic;
	signal s_move        : std_ulogic_vector (3 downto 0);
	
    signal s_WriteRegDest_muxout: std_ulogic_vector(4 downto 0);
	
    signal r_PC                    : std_ulogic_vector(31 downto 0);
    signal s_PC_Suivant            : std_ulogic_vector(31 downto 0);
    signal s_adresse_PC_plus_4     : std_ulogic_vector(31 downto 0);
    signal s_adresse_jump          : std_ulogic_vector(31 downto 0);
    signal s_adresse_branche       : std_ulogic_vector(31 downto 0);
    
    signal s_Instruction : std_ulogic_vector(31 downto 0);

    signal s_opcode      : std_ulogic_vector( 5 downto 0);
    signal s_RS          : std_ulogic_vector( 4 downto 0);
    signal s_RT          : std_ulogic_vector( 4 downto 0);
    signal s_RD          : std_ulogic_vector( 4 downto 0);
    signal s_shamt       : std_ulogic_vector( 4 downto 0);
    signal s_instr_funct : std_ulogic_vector( 5 downto 0);
    signal s_imm16       : std_ulogic_vector(15 downto 0);
    signal s_jump_field  : std_ulogic_vector(25 downto 0);
    
    signal s_reg_data1        : std_ulogic_vector(31 downto 0);
    signal s_reg_data2        : std_ulogic_vector(31 downto 0);
    signal s_reg_data3        : std_ulogic_vector(31 downto 0);
    signal s_reg_data4        : std_ulogic_vector(31 downto 0);
    signal s_reg_data5        : std_ulogic_vector(31 downto 0);
    signal s_reg_data6        : std_ulogic_vector(31 downto 0);
    signal s_reg_data7        : std_ulogic_vector(31 downto 0);
    signal s_reg_data8        : std_ulogic_vector(31 downto 0);
    
    signal s_AluResult             : std_ulogic_vector(31 downto 0);
    signal s_AluResultV1           : std_ulogic_vector(31 downto 0);
    signal s_AluResultV2           : std_ulogic_vector(31 downto 0);
    signal s_AluResultV3           : std_ulogic_vector(31 downto 0);
    signal s_AluResultVecteur      : std_ulogic_vector(127 downto 0);
    signal s_AluMultResult         : std_ulogic_vector(63 downto 0);
    
    signal s_Data2Reg_muxout       : std_ulogic_vector(31 downto 0);
    signal s_Data2Reg_muxout_vect  : std_ulogic_vector(127 downto 0);
    
    signal s_imm_extended          : std_ulogic_vector(31 downto 0);
    signal s_imm_extended_shifted  : std_ulogic_vector(31 downto 0);
	
    signal s_Reg_Wr_Data           : std_ulogic_vector(31 downto 0);
    signal s_MemoryReadData        : std_ulogic_vector(31 downto 0);
    signal s_MemoryReadDataWide    : std_ulogic_vector(127 downto 0);
    signal s_AluB_data             : std_ulogic_vector(31 downto 0);
    signal s_AluB1_data            : std_ulogic_vector(31 downto 0);
    signal s_AluB2_data            : std_ulogic_vector(31 downto 0);
    signal s_AluB3_data            : std_ulogic_vector(31 downto 0);
    
    -- registres sp�ciaux pour la multiplication
    signal r_HI             : std_ulogic_vector(31 downto 0);
    signal r_LO             : std_ulogic_vector(31 downto 0);
	

begin

o_PC	<= r_PC; -- permet au synth�tiseur de sortir de la logique. Sinon, il enl�ve tout...

------------------------------------------------------------------------
-- simplification des noms de signaux et transformation des types
------------------------------------------------------------------------
s_opcode        <= s_Instruction(31 downto 26);
s_RS            <= s_Instruction(25 downto 21);
s_RT            <= s_Instruction(20 downto 16);
s_RD            <= s_Instruction(15 downto 11);
s_shamt         <= s_Instruction(10 downto  6);
s_instr_funct   <= s_Instruction( 5 downto  0);
s_imm16         <= s_Instruction(15 downto  0);
s_jump_field	<= s_Instruction(25 downto  0);
------------------------------------------------------------------------


------------------------------------------------------------------------
-- Compteur de programme et mise � jour de valeur
------------------------------------------------------------------------
process(clk)
begin
    if(clk'event and clk = '1') then
        if(reset = '1') then
            r_PC <= X"00400000";
        else
            r_PC <= s_PC_Suivant;
        end if;
    end if;
end process;

s_adresse_PC_plus_4				<= std_ulogic_vector(unsigned(r_PC) + 4);
s_adresse_jump					<= s_adresse_PC_plus_4(31 downto 28) & s_jump_field & "00";
s_imm_extended_shifted			<= s_imm_extended(29 downto 0) & "00";
s_adresse_branche				<= std_ulogic_vector(unsigned(s_imm_extended_shifted) + unsigned(s_adresse_PC_plus_4));

-- note, "i_jump_register" n'est pas dans les figures de COD5
s_PC_Suivant		<= s_adresse_jump when i_jump = '1' else
                       s_reg_data1 when i_jump_register = '1' else
					   s_adresse_branche when (i_branch = '1' and s_zero = '1') else
					   s_adresse_PC_plus_4;
					   

------------------------------------------------------------------------
-- M�moire d'instructions
------------------------------------------------------------------------
inst_MemInstr: MemInstructions
Port map ( 
	i_addresse => r_PC,
    o_instruction => s_Instruction
    );

-- branchement vers le d�codeur d'instructions
o_instruction <= s_Instruction;
	
------------------------------------------------------------------------
-- Banc de Registres
------------------------------------------------------------------------
-- Multiplexeur pour le registre en �criture
s_WriteRegDest_muxout <= c_Registre31 when i_jump_link = '1' else 
                         s_rt         when i_RegDst = '0' else 
						 s_rd;
       
inst_Registres: BancRegistres 
port map ( 
	clk          => clk,
	reset        => reset,
	i_RS1        => s_rs,
	i_RS2        => s_rt,
	i_Wr_DAT     => s_Data2Reg_muxout,
	i_Wr_DAT_vecteur => s_Data2Reg_muxout_vect,
	i_WDest      => s_WriteRegDest_muxout,
	i_WE         => i_RegWrite,
	i_Move       => s_move,
	i_vecteur    => i_vecteur,
	o_RS1_DAT    => s_reg_data1,
	o_RS2_DAT    => s_reg_data2,
	o_RS3_DAT    => s_reg_data3,
	o_RS4_DAT    => s_reg_data4,
	o_RS5_DAT    => s_reg_data5,
	o_RS6_DAT    => s_reg_data6,
	o_RS7_DAT    => s_reg_data7,
	o_RS8_DAT    => s_reg_data8
	);
	

------------------------------------------------------------------------
-- ALU (instance, extension de signe et mux d'entr�e pour les imm�diats)
------------------------------------------------------------------------
-- extension de signe
s_imm_extended <= std_ulogic_vector(resize(  signed(s_imm16),32)) when i_SignExtend = '1' else -- extension de signe � 32 bits
				  std_ulogic_vector(resize(unsigned(s_imm16),32)); 

-- Mux pour imm�diats
s_AluB_data <= s_reg_data2 when i_ALUSrc = '0' else s_imm_extended;
s_AluB1_data <= s_reg_data4 when i_ALUSrc = '0' else s_imm_extended;
s_AluB2_data <= s_reg_data6 when i_ALUSrc = '0' else s_imm_extended;
s_AluB3_data <= s_reg_data8 when i_ALUSrc = '0' else s_imm_extended;
inst_Alu: alu 
port map( 
	i_a         => s_reg_data1,
	i_b         => s_AluB_data,
	i_alu_funct => i_alu_funct,
	i_shamt     => s_shamt,
	o_result    => s_AluResult,
	o_multRes   => s_AluMultResult,
	o_zero      => s_zero
	);
inst_Alu1: alu 
port map( 
	i_a         => s_reg_data3,
	i_b         => s_AluB1_data,
	i_alu_funct => i_alu_funct,
	i_shamt     => s_shamt,
	o_result    => s_AluResultV1
	--o_multRes   => s_AluMultResult
	--o_zero      => s_zero
	);
inst_Alu2: alu 
port map( 
	i_a         => s_reg_data5,
	i_b         => s_AluB2_data,
	i_alu_funct => i_alu_funct,
	i_shamt     => s_shamt,
	o_result    => s_AluResultV2
--	o_multRes   => s_AluMultResult
	--o_zero      => s_zero
	);
inst_Alu3: alu 
port map( 
	i_a         => s_reg_data7,
	i_b         => s_AluB3_data,
	i_alu_funct => i_alu_funct,
	i_shamt     => s_shamt,
	o_result    => s_AluResultV3
	--o_multRes   => s_AluMultResult
	--o_zero      => s_zero
	);

------------------------------------------------------------------------
-- M�moire de donn�es
------------------------------------------------------------------------
inst_MemDonneesWideDual : MemDonneesWideDual	
Port map( 
	clk 		=> clk,
	reset 		=> reset,
	i_MemRead	=> i_MemRead,
	i_MemWrite	=> i_MemWrite,
    i_Addresse	=> s_AluResult,
	i_WriteData => s_reg_data2,
    o_ReadData	=> s_MemoryReadData,
    i_MemReadWide => i_MemReadWide,
    i_MemWriteWide => i_MemWriteWide,
    i_WriteDataWide =>  s_reg_data8 & s_reg_data6 & s_reg_data4 & s_reg_data2,
    o_ReadDataWide => s_MemoryReadDataWide
	);
	
	inst_ResultVecteur : ResultVecteur
	Port map(
	 i_MIPS_funct  => s_instr_funct,
	 rotv      => i_rotation,
     resALU1   => s_AluResult,
     resALU2   => s_AluResultV1,
     resALU3   => s_AluResultV2,
     resALU4   => s_AluResultV3,
     resVecteur => s_AluResultVecteur,
     o_Move    => s_move
	);
------------------------------------------------------------------------
-- Mux d'�criture vers le banc de registres
------------------------------------------------------------------------
--s_AluResultVecteur <= s_AluResultV3 & s_AluResultV2 & s_AluResultV1 & s_AluResult;
s_Data2Reg_muxout    <= s_adresse_PC_plus_4 when i_jump_link = '1' else
					    r_HI                when i_mfhi = '1' else 
					    r_LO                when i_mflo = '1' else
					    s_AluResult         when i_MemtoReg = '0' else 
						s_MemoryReadData    when i_MemRead = '1' 
						;
s_Data2Reg_muxout_vect <= s_MemoryReadDataWide when i_MemReadWide = '1' else
                         s_AluResultVecteur;

		
------------------------------------------------------------------------
-- Registres sp�ciaux pour la multiplication
------------------------------------------------------------------------				
process(clk)
begin
    if(clk'event and clk = '1') then
        if(i_alu_mult = '1') then
            r_HI <= s_AluMultResult(63 downto 32);
            r_LO <= s_AluMultResult(31 downto 0);
        end if;
    end if;
end process;
        
end Behavioral;
