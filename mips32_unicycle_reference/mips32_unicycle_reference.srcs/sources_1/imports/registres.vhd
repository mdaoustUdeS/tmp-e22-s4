---------------------------------------------------------------------------------------------
--
--	Universit� de Sherbrooke 
--  D�partement de g�nie �lectrique et g�nie informatique
--
--	S4i - APP4 
--	
--
--	Auteur: 		Marc-Andr� T�trault
--					Daniel Dalle
--					S�bastien Roy
-- 
---------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.MIPS32_package.all;

entity BancRegistres is
    Port ( clk       : in  std_ulogic;
           reset     : in  std_ulogic;
           i_RS1     : in  std_ulogic_vector (4 downto 0);
           i_RS2     : in  std_ulogic_vector (4 downto 0);
           i_Wr_DAT  : in  std_ulogic_vector (31 downto 0);
           i_Wr_DAT_vecteur : in  std_ulogic_vector (127 downto 0);
           i_WDest   : in  std_ulogic_vector (4 downto 0);
           i_WE 	 : in  std_ulogic;
           i_Move 	 : in  std_ulogic_vector (3 downto 0);
           i_vecteur : in std_ulogic;
           o_RS1_DAT : out std_ulogic_vector (31 downto 0);
           o_RS2_DAT : out std_ulogic_vector (31 downto 0);
           o_RS3_DAT : out std_ulogic_vector (31 downto 0);
           o_RS4_DAT : out std_ulogic_vector (31 downto 0);
           o_RS5_DAT : out std_ulogic_vector (31 downto 0);
           o_RS6_DAT : out std_ulogic_vector (31 downto 0);
           o_RS7_DAT : out std_ulogic_vector (31 downto 0);
           o_RS8_DAT : out std_ulogic_vector (31 downto 0));
end BancRegistres;

architecture comport of BancRegistres is
    signal regs: RAM(0 to 31) := (29 => X"100103FC", -- registre $SP
                                others => (others => '0'));
begin
    process( clk,  i_Move)
    begin
        if clk='1' and clk'event then
            if i_WE = '1' and reset = '0' and i_WDest /= "00000" and i_vecteur = '0' and i_Move = "1111"then
                regs( to_integer( unsigned(i_WDest))) <= i_Wr_DAT;
            elsif i_WE = '1' and reset = '0' and i_WDest /= "00000" and i_vecteur = '1' then
                if(i_Move(3) = '1') then 
                regs( to_integer( unsigned(i_WDest))+3) <= i_Wr_DAT_vecteur(127 downto 96);
                end if;
                if(i_Move(2) = '1') then 
                regs( to_integer( unsigned(i_WDest))+2) <= i_Wr_DAT_vecteur(95 downto 64);
                end if;
                if(i_Move(1) = '1') then 
                regs( to_integer( unsigned(i_WDest))+1) <= i_Wr_DAT_vecteur(63 downto 32);
                end if;
                if(i_Move(0) = '1') then 
                regs( to_integer( unsigned(i_WDest))) <= i_Wr_DAT_vecteur(31 downto 0);
                end if;
            end if;
        end if;
    end process;
    
    o_RS1_DAT <= regs( to_integer(unsigned(i_RS1)));
    o_RS2_DAT <= regs( to_integer(unsigned(i_RS2)));
    o_RS3_DAT <= regs( to_integer(unsigned(i_RS1))+1);
    o_RS4_DAT <= regs( to_integer(unsigned(i_RS2))+1);
    o_RS5_DAT <= regs( to_integer(unsigned(i_RS1))+2);
    o_RS6_DAT <= regs( to_integer(unsigned(i_RS2))+2);
    o_RS7_DAT <= regs( to_integer(unsigned(i_RS1))+3);
    o_RS8_DAT <= regs( to_integer(unsigned(i_RS2))+3);
    
end comport;

