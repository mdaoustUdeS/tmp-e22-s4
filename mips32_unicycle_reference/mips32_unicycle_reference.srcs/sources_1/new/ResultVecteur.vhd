----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.06.2022 13:04:26
-- Design Name: 
-- Module Name: ResultVecteur - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.MIPS32_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ResultVecteur is
     Port ( 
       i_MIPS_funct  : in std_ulogic_vector (5 downto 0);
       rotv : in std_ulogic := '0';
       resALU1 : in std_ulogic_vector (31 downto 0) := (others => '0');
       resALU2 : in std_ulogic_vector (31 downto 0) := (others => '0');
       resALU3 : in std_ulogic_vector (31 downto 0) := (others => '0');
       resALU4 : in std_ulogic_vector (31 downto 0) := (others => '0');
       resVecteur : out std_ulogic_vector (127 downto 0);-- := (others => '0');
       o_Move   : out std_ulogic_vector(3 downto 0) := "1111");
end ResultVecteur;

architecture Behavioral of ResultVecteur is

begin
v1: process(i_MIPS_funct, resALU1, resALU2, resALU3, resALU4)
begin
--resVecteur <= resALU4 & resALU3 & resALU2 & resALU1;

   if (rotv = '1') then 
            resVecteur <= resALU1 & resALU4 & resALU3 & resALU2; 
            o_Move <= "1111";
   else
    case i_MIPS_funct is
            when ALUF_SLTV => 
                resVecteur <= resALU4 & resALU3 & resALU2 & resALU1;
                  o_Move <= "1111";
            when ALUF_MOVZV =>
                o_Move <= resALU4(0 downto 0) & resALU3(0 downto 0) & resALU2(0 downto 0) & resALU1(0 downto 0);
                resVecteur <= resALU4 & resALU3 & resALU2 & resALU1;
            when ALUF_MOVNV =>
                o_Move <= resALU4(0 downto 0) & resALU3(0 downto 0) & resALU2(0 downto 0) & resALU1(0 downto 0);
                resVecteur <= resALU4 & resALU3 & resALU2 & resALU1;
                
            when ALUF_ADDV =>
                resVecteur <= resALU4 & resALU3 & resALU2 & resALU1; 
                o_Move <= "1111";
            when others => 
                resVecteur <= (others => '0');
                o_Move <= "1111";
        end case;
        end if;
end process;

end Behavioral;
