###########################################
# Prénom:	Jérémy
# Nom:		Cusson
# CIP:		cusj3102
# Date:		2 juillet 2022
###########################################

.data

# Valeur recherché pour soutput {2, 12, 20, 28} -> trouver avec calcul

so_vecteur: .word 0,0,0,0
in_met: .word 2,4,6,1,10,12,14,16,18,20,22,24,26,28,30,32 # Metrique = A[i][j]
si_vecteur: .word 2,4,6,1 # x[j]
.text	0x400000

.globl main

main:
	addiu $sp, $sp, -8		# Protection pour la pile
	sw $s0, 0($sp)			# $s0 protege par la pile
	sw $s1, 4($sp)			# $s1 protege par la pile
	sw $s2, 8($sp)			# $s2 protege par la pile

	li $s0, 0			# Représente le i de la boucle externe et est egale a 0
	li $s1, 4			# 4 represante la limite des boucles

CalculSurvivants:
	beq $s0, $s1, end		# Si i = 4, on saute à end
	li $s2, 0			# Reprsante le j de la boucle interne et est egale a 0
	addi $t0, $zero, 250		# y[i] = 0 + 250
	j acs				# Saute a la fonction acs vue dans le code C

acs:
	beq $s2, $s1, SortieACS		# Si j = 4, on saute à CalculSuivants

	mul $t1, $s2, 4			# Permet de se deplacer dans le vecteur si_vecteur par octect, car un montant de decalage 2
					# revient a faire j*4, ce qui permet plus tard de voyager dans les colonnes du vecteur x[j]
	lw $t2, si_vecteur($t1)		# lecture de x[j], ou pour la position est a j*4 (octet)

	mul $t1, $s0, 4			# Permet de se deplacer dans la matrice in_met par octect au niveau du i, car un montant de decalage 2
					# revient a faire *4, ce qui permet plus tard de voyager dans les lignes de la matrice in_met[i][j]

	add $t1, $t1, $s2		# i*4+j

	mul $t1, $t1, 4			# Permet le deplacement en octect: necessaire a cause de l'ajout du j
					# formul i*4+j pour trouver la position voulu dans une matrice (in_met), trouver à l'aide du document ReferenceStandard_ProduitMatrice.asm
	lw $t3, in_met($t1)		# lecture de A[i][j]

	add $t4, $t3, $t2		# temp = A[i][j] + x[j]

	slt $t5, $t4, $t0		# Regarde si temp < y[i]

	movn $t0, $t4, $t5		# Si temp < y[i] = 1, y[i] = temp

	addi $s2, $s2, 1		# j = j + 1
	j acs				# retourne à acs

SortieACS:
	mul $t1, $s0, 4
	sw $t0, so_vecteur($t1)		# Ecrit le y[i]
	addi $s0, $s0, 1		# i = i + 1
	j CalculSurvivants		# retourne à acs

end:
	lw $s0, 0($sp)			# $s0 protege par la pile
	lw $s1, 4($sp)			# $s1 protege par la pile
	lw $s2, 8($sp)			# $s2 protege par la pile
	addiu $sp, $sp, 8		# Restoration de la pile

	li $v0, 10			# Fin
	syscall

	# Valeurs de sortie obtenues... hexadecimal: 2, c, 14, 1c... decimal: 2, 12, 20, 28
